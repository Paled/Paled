#include <derevel.h>

void task_build_debug(String arg);
void task_build_release(String arg);
void task_install(String arg);
void task_run(String arg);
void task_clean(String arg);

void task_build_example(String arg);

// Register build tasks
void registerTasks(Map *tasks) {
    addTask(tasks, "debug", task_build_debug, NULL);
    addTask(tasks, "release", task_build_release, NULL);
    addTask(tasks, "install", task_install, "Installs into given directory, ~/bin/ by default");
    addTask(tasks, "run", task_run, "Run the program");
    addTask(tasks, "clean", task_clean, "Delete build directory");
    
    addTask(tasks, "example", task_build_example, "Builds the example program");
}

// Runs when no task is specified
// Automatically registered by derevel
void task_default(String arg) {
    logInfo("Task", "Default");
    
    task_build_debug(arg);
    task_build_release(arg);
    task_build_example(arg);
}

// Derevel hook that automatically runs after Derevel
// compiles this build file
// Automatically registered by derevel
void task_derevel(String arg) {
    task_default(arg);
}

// Build debug binary
void task_build_debug(String arg) {
    logInfo("Task", "Debug");
    makeDirectory("build/debug");
    
    String *defines = NULL;
    buf_push(defines, cStrToBuffer("DEBUG"));
#ifndef _WIN32
    buf_push(defines, cStrToBuffer("_XOPEN_SOURCE=500"));
#endif
    
    String *files = NULL;
    buf_push(files, cStrToBuffer("src/main.c"));
    buf_push(files, cStrToBuffer("src/stretchybuffer.c"));
    buf_push(files, cStrToBuffer("src/hashmap.c"));
    buf_push(files, cStrToBuffer("src/colors.c"));
    buf_push(files, cStrToBuffer("src/parsing.c"));
    buf_push(files, cStrToBuffer("src/string.c"));
    
    String outputFile = cStrToBuffer("./build/debug/paled");
    
    // @TODO: Defines, debug vs. release options, includes, output file
    compile(C_99, MODE_DEBUG, defines, files, NULL, outputFile);
}

void task_build_release(String arg) {
    logInfo("Task", "Release");
    makeDirectory("build/release");
    
    String *defines = NULL;
#ifndef _WIN32
    buf_push(defines, cStrToBuffer("_XOPEN_SOURCE=500"));
#endif
    
    String *files = NULL;
    buf_push(files, cStrToBuffer("src/main.c"));
    buf_push(files, cStrToBuffer("src/stretchybuffer.c"));
    buf_push(files, cStrToBuffer("src/hashmap.c"));
    buf_push(files, cStrToBuffer("src/colors.c"));
    buf_push(files, cStrToBuffer("src/parsing.c"));
    buf_push(files, cStrToBuffer("src/string.c"));
    
    String outputFile = cStrToBuffer("./build/release/paled");
    
    // @TODO: Defines, debug vs. release options, includes, output file
    compile(C_99, MODE_RELEASE, defines, files, NULL, outputFile);
}

void task_install(String arg) {
    logInfo("Task", "Install");
    task_build_release(NULL);
    
    String directory = NULL;
    if (arg) {
        directory = arg;
        buf_push(directory, '\0');
        logInfo("Install", "Installing in given directory '%s'", directory);
    } else {
        directory = cStrToString("~/bin/");
        buf_push(directory, '\0');
    }
    
    makeDirectory(directory); // @TODO: Handle '~' in makeDirectory
    
    // Copy executable to directory
    copyFile("build/release/paled", directory);
    
#ifndef _WIN32
    // Strip executable
    String cliCommand = NULL;
    String_appendCString(&cliCommand, "strip ");
    String_appendString(&cliCommand, directory);
    buf_pop(cliCommand); // Pop off the null terminator from directory
    if (cliCommand[buf_len(cliCommand) - 1] != '/') {
        buf_push(cliCommand, '/');
    }
    String_appendCString(&cliCommand, "paled");
    buf_push(cliCommand, '\0');
    system(cliCommand);
#endif
}

void task_run(String arg) {
    logInfo("Task", "Run");
    size_t argLength = buf_len(arg);
    
    if (argLength >= 5 && strncmp(arg, "debug", MAX(argLength, 5)) == 0) {
        logInfo("Run", "Running debug build.");
#ifdef _WIN32
        system("build\\debug\\paled");
#else
        system("build/debug/paled");
#endif
    } else if (argLength >= 7 && strncmp(arg, "release", MAX(argLength, 7)) == 0) {
        logInfo("Run", "Running release build.");
#ifdef _WIN32
        system("build\\release\\paled");
#else
        system("build/release/paled");
#endif
    } else {
        logInfo("Run", "Running debug build.");
#ifdef _WIN32
        system("build\\debug\\paled");
#else
        system("build/debug/paled");
#endif
    }
}

void task_clean(String arg) {
    logInfo("Task", "Clean");
#ifdef _WIN32
    system("rmdir /Q /S build"); // @TODO: Hacky
#else
    system("rm -rf build");
#endif
}

void task_build_example(String arg) {
    logInfo("Task", "Example");
    makeDirectory("build/release");
    
    String *defines = NULL;
#ifndef _WIN32
    buf_push(defines, cStrToBuffer("_XOPEN_SOURCE=500"));
#endif
    
    String *files = NULL;
    buf_push(files, cStrToBuffer("example_src/main.c"));
    buf_push(files, cStrToBuffer("src/stretchybuffer.c"));
    buf_push(files, cStrToBuffer("src/hashmap.c"));
    buf_push(files, cStrToBuffer("src/colors.c"));
    buf_push(files, cStrToBuffer("src/parsing.c"));
    buf_push(files, cStrToBuffer("src/string.c"));
    
    String outputFile = cStrToBuffer("./build/release/example");
    
    // @TODO: Defines, debug vs. release options, includes, output file
    compile(C_99, MODE_RELEASE, defines, files, NULL, outputFile);
}
