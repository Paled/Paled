#include <stdio.h>

#include "../src/paled.h"

int main(int argc, char **argp) {
    printf("Hello, World!\n");
    printf("Argc: %d, Argp: %p\n", argc, argp);
    printf("buf_len(argp): %d", buf_len(argp));
    
    printf("\nArgs:\n");
    for (int i = 0; i < argc; i++) {
        printf("'%s'\n", argp[i]);
    }
    
    return(0);
}