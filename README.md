# Paled

Paled uses **Derevel** to generate an executable from a `build.c` file to build the program. (Note: Windows support for Derevel is coming soon)

## To Compile
Run `./build_linux` to compile the program for linux - both debug and release binaries.

If you would like to change the way this program is built, edit the `build.c` file, then run `derevel`.

## To Run
Debug: `./build_linux run`
release: `./build_linux run release`
