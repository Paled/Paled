#ifndef HEADER_DECONFLICTER
#define HEADER_DECONFLICTER

#ifndef __APPLE__
#include <malloc.h>
#endif

#include <stdio.h>
#include <ctype.h>
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#ifdef _WIN32
// Windows
#include <conio.h>
#define getch _getch
#define kbhit _kbhit
#else
// Unix
#include <alloca.h>
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <pthread.h>
char getch();
char getch_nonblocking();
#endif

#define byte char
#define ubyte unsigned char
#define forever for(;;)

#define internal static

#ifdef _WIN32
#define THREAD_RETURN_T void
#else
#define THREAD_RETURN_T void *
#endif

#define LOG_TYPE ubyte
#define LOG_ERROR 0
#define LOG_DEBUG 1
#define LOG_INFO 2

typedef struct TermSize {
	int width;
	int height;
} TermSize;

void clrscr();
TermSize getTermSize();

// GCC only supports ##__VA_ARGS__, which removes trailing ',' if varargs are empty
// Visual Studio automaticlaly gets rid of trailing commas. Clang?
#define logInfo(a, b, ...) printInfo("[INFO:%s]  ", (a)); printf((b), ##__VA_ARGS__); printf(" \n");

#ifdef DEBUG
#define DBG(x) x
#define logDebug(a, b, ...) printDebug("[DEBUG:%s] ", (a)); printf((b), ##__VA_ARGS__); printf(" (%s: %u)\n", __FILE__, __LINE__);
#define logError(a, b, ...) printError("[ERROR:%s] ", (a)); printf((b), ##__VA_ARGS__); printf(" (%s: %u)\n", __FILE__, __LINE__);
#else
#define DBG(x) ;
#define logDebug ;
#define logError(a, b, ...) printError("[ERROR:%s] ", (a)); printf((b), ##__VA_ARGS__); printf(" \n");
#endif


#define vertical "\u2502"
#define vertical_and_left "\u2524"
#define vertical_and_right "\u251C"

#define horizontal "\u2500"
#define horizontal2 horizontal horizontal
#define horizontal_and_down "\u252C"
#define horizontal_and_up "\u2534"
#define heavy_horizontal_and_down "\u252F"
#define heavy_horizontal_and_up "\u2537"
#define heavy_horizontal_vertical "\u253F"

#define double_horizontal "\u2550"
#define double_horizontal2 double_horizontal double_horizontal
#define double_horizontal3 double_horizontal double_horizontal double_horizontal
#define double_horizontal_vertical "\u256A"


/* == Hashmap (from BitWise tutorial series) == */
// Open-addressed HashTable
// Linear Probing
// Optimized for hole-in-one (at most one cache-miss)
// When growing, makes sure load factor is never more than 50%
// Key of '0' not allowed
// Deletions unsupported

uint64_t hash_uint64(uint64_t x);
uint64_t hash_ptr(const void *prt);
uint64_t hash_mix(uint64_t x, uint64_t y);
uint64_t hash_bytes(const void *ptr, size_t len);

typedef struct Map {
	uint64_t *keys;
	uint64_t *vals;
	size_t len;
	size_t cap;
} Map;

uint64_t map_get_uint64_from_uint64(Map *map, uint64_t key);
void map_put_uint64_from_uint64(Map *map, uint64_t key, uint64_t val);
void map_grow(Map *map, size_t new_cap);
void *map_get(Map *map, const void *key);
void map_put(Map *map, const void *key, void *val);
void *map_get_from_uint64(Map *map, uint64_t key);
void map_put_from_uint64(Map *map, uint64_t key, void *val);
uint64_t map_get_uint64(Map *map, void *key);
void map_put_uint64(Map *map, void *key, uint64_t val);
void map_test(void);


/* == Stretchy Buffers (originally by Sean Barratt) == */
#define MAX(x, y) ((x) >= (y) ? (x) : (y))
#define MIN(x, y) ((x) <= (y) ? (x) : (y))
#define CLAMP_MAX(x, max) MIN(x, max)
#define CLAMP_MIN(x, min) MAX(x, min)
#define IS_POW2(x) (((x) != 0) && ((x) & ((x)-1)) == 0)

void *xcalloc(size_t num_elems, size_t elem_size);
void *xrealloc(void *prt, size_t num_bytes);
void *xmalloc(size_t num_bytes);
void fatal(const char *fmt, ...);
void *buf_copy(void *buf, size_t elem_size);

// @TODO(Christian): Make a buffer specifically for static strings that doesn't need a capacity?
typedef struct BufHdr {
	size_t len;
	size_t cap;
	char buf[0]; // [0] new in C99
} BufHdr;


// Internal, Get the header of the buffer
#define buf__hdr(b) ((BufHdr *) ((char *) (b)  - offsetof(BufHdr, buf)))

// Internal, Returns true if number of elements fits into currently allocated memory
#define buf__fits(b, n) (buf_len(b) + (n) <= buf_cap(b))

// Internal, If number of elements added doesn't fit into currently allocated memory, the buffer will grow.
// Otherwise, 0 is returned.
#define buf__fit(b, n) (buf__fits((b), (n)) ? 0 : ((b) = buf__grow((b), buf_len(b) + (n), sizeof(*(b)))))


// Get the length of the buffer
#define buf_len(b) ((b) ? buf__hdr(b)->len : 0)

// Get the total capacity (amount of currently allocated memory) of buffer
#define buf_cap(b) ((b) ? buf__hdr(b)->cap : 0)

// Push an element onto the end of the buffer
#define buf_push(b, x) (buf__fit((b), 1), (b)[buf__hdr(b)->len++] = (x))

// Get the last element of the buffer
#define buf_end(b) ((b) + buf_len(b))


// Add n elements onto the end of the buffer all at once
#define buf_add(b, n) (buf__fit((b), n), buf__hdr(b)->len += n, &(b)[buf__hdr(b)->len - n]) // TODO: Not sure if I should be returning the address or not

// Pop off and return the last element of the buffer
#define buf_pop(b) (buf__hdr(b)->len--, &(b)[buf__hdr(b)->len + 1]) // TODO: Check that array exists and length doesn't go below 0

// Pop off all of the elements of the buffer
#define buf_pop_all(b) ((b) ? (buf__hdr(b)->len = 0) : 0)


// Free the buffer (including the header)
#define buf_free(b) ((b) ? (free(buf__hdr(b)), (b) = NULL) : 0)

// Internal, Grow the buffer to fix new length
void *buf__grow(const void *buf, size_t new_len, size_t elem_size);


// Convert null-terminated C-Strings to stretchy buffer (leaving off '\0')
// @TODO: Allow varargs
char *cStrToBuffer(char const *str);

char *strFormatToBuffer(char const *format, ...);

// Convert array of characters to stretchy buffer
char *strToBuffer(char const *str, size_t strLength);


/* == String.c == */
#define String char *
#define cStrToString(strp) cStrToBuffer((strp))
#define strFormatToString(format, ...) strFormatToBuffer((format), ##__VA_ARGS__)
void String_appendCString(String *buffer, char *str2);
void String_appendString(String *buffer, String str2);


/* == Colors == */
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#define FOREGROUND_YELLOW FOREGROUND_RED|FOREGROUND_GREEN
#define FOREGROUND_CYAN FOREGROUND_GREEN|FOREGROUND_BLUE
#define FOREGROUND_MAGENTA FOREGROUND_RED|FOREGROUND_BLUE
#define FOREGROUND_WHITE FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE

#define BACKGROUND_YELLOW BACKGROUND_RED|BACKGROUND_GREEN
#define BACKGROUND_CYAN BACKGROUND_GREEN|BACKGROUND_BLUE
#define BACKGROUND_MAGENTA BACKGROUND_RED|BACKGROUND_BLUE
#define BACKGROUND_WHITE BACKGROUND_RED|BACKGROUND_GREEN|BACKGROUND_BLUE

#endif

#define COL_RED "\x1b[31m" // Error
#define COL_GREEN "\x1b[32m" // Prompt, Success?
#define COL_YELLOW "\x1b[33m" // Line Numbers
#define COL_BLUE "\x1b[34m"
#define COL_MAGENTA "\x1b[35m"
#define COL_CYAN "\x1b[36m" // Information
#define COL_RESET "\x1b[0m" // Input

#define COLOR ubyte
#define COLOR_RED 0
#define COLOR_YELLOW 1
#define COLOR_GREEN 2
#define COLOR_MAGENTA 3
#define COLOR_BLUE 4
#define COLOR_CYAN 5
#define COLOR_WHITE 6
#define COLOR_BLACK 7

#ifdef _WIN32
HANDLE hConsole; // Used for coloring output on Windows
#endif

void setColor(COLOR foreground);
void resetColor(void);

void colors_printf(COLOR foreground, const char *fmt, ...);
void colors_sprintf(COLOR foreground, char *str, const char *fmt, ...);
void colors_puts(COLOR foreground, const char *str);
void colors_stdout_fputs(COLOR foreground, const char *str);
void colors_putchar(COLOR foreground, char c);

void printError(const char *fmt, ...);
void printInfo(const char *fmt, ...);
void printPrompt(const char *fmt, ...);
void printDebug(const char *fmt, ...);


/* == parsing.c == */

typedef struct pString {
	char *start;
	char *end;
} pString;

// Function pointer to function that can run user-code on specific keypresses during input (with getInput). If null, the function is not called
// Return true if keypress should continue to use default action provided by getInput()
typedef bool (*inputKeyCallback)(char, bool isSpecial, char **, int *);

#define INPUT_ESC 27

// ANSI Control Characters
#define INPUT_CTRL_L 12 // Clear Scrren
#define INPUT_CTRL_X 24 // Cancel
#define INPUT_CTRL_C 3 // Currently: Exit Program, TODO: Exit or Copy?
#define INPUT_CTRL_O 15

// Special Keys
#ifdef _WIN32
#define INPUT_SPECIAL1 -32
#define INPUT_SPECIAL2 224 // TODO
#define INPUT_LEFT 75
#define INPUT_UP 76
// TODO: Fix
#define INPUT_RIGHT 77
#define INPUT_DOWN 78
// TODO: Fix
#define INPUT_DELETE 83
#define INPUT_END 79
#define INPUT_HOME 71
#define INPUT_ENDINPUT 26 // CTRL-Z
#define INPUT_BACKSPACE 8
#else
#define INPUT_SPECIAL1 27
#define INPUT_SPECIAL2 91
#define INPUT_LEFT 68
#define INPUT_RIGHT 67
#define INPUT_UP 65
#define INPUT_DOWN 66
#define INPUT_DELETE1 51
#define INPUT_DELETE2 126
#define INPUT_END 70
#define INPUT_HOME 72
#define INPUT_ENDINPUT 4 // CTRL-D
#define INPUT_BACKSPACE 127
#endif

char *skipWhitespace(char *start);
char *skipWord(char *start, bool includeNumbers, bool includeSymbols);
char *skipNumbers(char *start);

int parsing_getLine(char *line, int max, int trimSpace);

// atoi with provided length
int antoi(const char *str, int len);

#endif
