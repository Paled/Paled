#include "paled.h"

// TODO:
// Path hashing
//  - If a command is in the path and calling it for the first time,
//    hash the command, setting the value to the path.
// Shell Startup
//  - Cache the path
//  - Cache the current directory?
//  - Hash all the executables that are in the current directory
// Path Change
//  - Recache the path
//  - Clear the path hashtable
// Deleting/Moving files that are in the path
//  - Clear the path hashtable if files are executables
//  - Clear the current directory hashtable if files are in current
//    directory *and* they are executables
// Changing Directory
//  - Cache the new current directory?
//  - Hash all the executables that are in the current directory
// Calling a command
//  - First check the current directory hashtable
//  - Then check the path hash table
//  - Then check the path, hashing if in there
//  - Return error if not found

#ifdef _WIN32

#include <conio.h>
#define getch _getch
#define kbhit _kbhit

// Hack for clearing screen for Windows // TODO: Improve this
void clrscr() {
    system("cls");
}

TermSize getTermSize() {
	// TODO
	return { 0, 0 };
}

#else

#include <unistd.h>  //_getch
#include <termios.h> //_getch
#include <sys/ioctl.h>

#include <sys/types.h> 
#include <sys/wait.h> 

char getch() {
    char buf=0;
    struct termios old={0};
    fflush(stdout);
    if(tcgetattr(0, &old)<0)
        perror("tcsetattr()");
    old.c_lflag&=~ICANON;
    old.c_lflag&=~ECHO;
    old.c_cc[VMIN]=1;
    old.c_cc[VTIME]=0;
    if(tcsetattr(0, TCSANOW, &old)<0)
        perror("tcsetattr ICANON");
    if(read(0,&buf,1)<0)
        perror("read()");
    old.c_lflag|=ICANON;
    old.c_lflag|=ECHO;
    if(tcsetattr(0, TCSADRAIN, &old)<0)
        perror ("tcsetattr ~ICANON");
    return buf;
}

char getch_nonblocking() {
    /*#include <unistd.h>   //_getch*/
    /*#include <termios.h>  //_getch*/
    char buf=0;
    struct termios old={0};
    fflush(stdout);
    if(tcgetattr(0, &old)<0)
        perror("tcsetattr()");
    old.c_lflag&=~ICANON;
    old.c_lflag&=~ECHO;
    old.c_cc[VMIN]=0;
    old.c_cc[VTIME]=0;
    if(tcsetattr(0, TCSANOW, &old)<0)
        perror("tcsetattr ICANON");
    if(read(0,&buf,1)<0)
        perror("read()");
    old.c_lflag|=ICANON;
    old.c_lflag|=ECHO;
    if(tcsetattr(0, TCSADRAIN, &old)<0)
        perror ("tcsetattr ~ICANON");
    return buf;
}

void clrscr() {
    printf("\e[1;1H\e[2J"); // TODO: Check whether actually portable
}

TermSize getTermSize() {
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	
	TermSize size = { w.ws_col, w.ws_row };
	return size;
}

#endif

typedef struct GlobalData {
    char *username;
    char *workingDirectory; // @Unused
} GlobalData;

// typedef int (*commandFunc)(int argc, char *args);
typedef void (*commandFunc)(char *args, char *endBound);
void command_ls(char *args, char *endBound);
void command_cd(char *args, char *endBound);
void command_commands(char *args, char *endBound);
void command_clear(char *args, char *endBound);
void command_exit(char *args, char *endBound);

void addCommand(Map *commands, const char *name, commandFunc function) {
    uint64_t hash = hash_bytes(name, strlen(name) * sizeof(char));
    //logDebug("Logging", "Add Command: '%s', %" PRIu64 ", %p", name, hash, function);
    map_put_from_uint64(commands, hash, (void *) function);
    
    //debugCommandFunc get_function = (commandFunc) map_get_from_uint64(commands, hash);
    //logDebug("Logging", "Test Get Command: '%s', %p == %p", name, function, get_function);
}

// Add commands hashmap with function pointers
void addCommands(Map *commands) {
    addCommand(commands, "ls", command_ls);
    addCommand(commands, "cd", command_cd);
    addCommand(commands, "builtins", command_commands);
    addCommand(commands, "help", command_commands);
    addCommand(commands, "?", command_commands);
    
    addCommand(commands, "clear", command_clear);
    
    addCommand(commands, "exit", command_exit);
}

void executeCommand(Map *commands, char *input) {
    char *current = input;
    char *endBound = buf_end(input);
    
    if (buf_len(input) == 0) {
        // strncmp doesn't work well with null pointers even if size is zero. We
        // actually handle another case in this if: the command might be empty
        printf("\n");
        return;
    }
    
    pString command;
    
    // Skip whitespace @TODO: Switch to my parsing.c version once I get that back in here
    while (current < endBound && (*current == ' ' || *current == '\t')) ++current;
    
    command.start = current;
    while (current < endBound && *current != ' ' && *current != '\n' && *current != '\r') {
        ++current;
    }
    command.end = current;
    size_t command_length = command.end - command.start;
    
    uint64_t command_hash = hash_bytes(command.start, command_length * sizeof(char));
    //printf("\n%" PRIu64 "\n", command_hash);
    
    commandFunc function = map_get_from_uint64(commands, command_hash);
    if (function != NULL) {
        function(command.end, endBound);
        return;
    } else {
        char **args = NULL;
        String file = NULL;
        if (command.start[0] != '/') {
            char *current_directory = getcwd(NULL, 0);
            String_appendCString(&file, current_directory);
            buf_push(file, '/');
        }
        
        for (int i = 0; i < command_length; i++)
            buf_push(file, command.start[i]);
        
        //String file = strToBuffer(command.start, command_length);
        buf_push(file, '\0');
        
        buf_push(args, file);
        buf_push(args, NULL);
        
        //printf("?\n");
        
        // Execute program
        pid_t pid, wpid;
        int status;
        
        pid = fork();
        if (pid == 0) {
            // Child process
            if (execvp(args[0], args) == -1) {
                //perror("lsh");
                logError("execute", "Error");
            }
            exit(EXIT_FAILURE);
        } else if (pid < 0) {
            // Error forking
            //perror("lsh");
            logError("execute", "Error forking");
        } else {
            // Parent process
            do {
                wpid = waitpid(pid, &status, WUNTRACED);
            } while (!WIFEXITED(status) && !WIFSIGNALED(status));
        }
    }
}

bool isSpecial(char c) {
#ifdef _WIN32
    if (c == INPUT_SPECIAL1 || c == INPUT_SPECIAL2) {
        return true;
    }
#else
    if (c == INPUT_SPECIAL1) {
        char next = getch();
        if (next == INPUT_SPECIAL2) {
            return true;
        }
    }
#endif
    
    return false;
}

void cli(GlobalData *globalData) {
    Map commands = {0};
    addCommands(&commands);
    
    char *input = NULL;
    char **input_history = NULL;
    int history_index = 0; // 0 == none, 1 == buf_len(input_history) - 1
    
    while (true) {
        char *current_dir = getcwd(NULL, 0);
        
        putchar('\r');
        printPrompt("%.*s: %s> ", buf_len(globalData->username), globalData->username, current_dir);
        printf("%.*s", (int)buf_len(input), input);
        
        char c;
        if (c = getch()) {
            if (c == INPUT_BACKSPACE) {
                if (buf_len(input) > 0) {
                    buf_pop(input);
                    //if (!reprintInput) {
                    printf("\b \b");
                    //}
                }
            } else if (c == '\n') {
                printf("\n\n");
                executeCommand(&commands, input);
                printf("\n");
                buf_push(input_history, input);
                input = NULL;
                history_index = 0;
                //reprintInput = true;
            } else if (isSpecial(c)) {
                c = getch();
                bool upOrDown = false;
                
                if (c == INPUT_UP) {
                    upOrDown = true;
                    if (history_index < buf_len(input_history))
                        ++history_index;
                } else if (c == INPUT_DOWN) {
                    upOrDown = true;
                    if (history_index != 0)
                        --history_index;
                }
                
                if (upOrDown) {
                    int spaces_count = buf_len(input);
                    if (history_index > 0 && history_index <= buf_len(input_history)) {
                        buf_pop_all(input);
                        char *hist_buf = input_history[buf_len(input_history) - history_index];
                        for (int i = 0; i < buf_len(hist_buf); i++) {
                            buf_push(input, hist_buf[i]);
                        }
                    } else if (history_index <= 0) {
                        buf_pop_all(input);
                    }
                    
                    // Print spaces to clear the word from the terminal
                    for (int i = 0; i < spaces_count; i++) {
                        fputs("\b \b", stdout);
                    }
                    //reprintInput = true;
                }
            } else if (c >= 32 && c <= 126) {
                buf_push(input, c);
                //if (!reprintInput) {
                printf("%c", c);
                //}
            }
        }
    }
}

#include <dirent.h>
#include <sys/dir.h>

void command_ls(char *args, char *endBound) {
    char *current_dir = getcwd(NULL, 0);
    DIR *dp;
    struct dirent *dirp;
    
    if ((dp = opendir(current_dir)) == NULL) {
        logError("ls", "Cannot open directory");
    }
    
    while ((dirp = readdir(dp)) != NULL) {
        printf("%s\t", dirp->d_name);
    }
    
    closedir(dp);
    free(current_dir);
}

void command_commands(char *args, char *endBound) {
    printf("Builtins: \n");
    printf("* ls\n");
    printf("* cd\n");
    printf("* find\n");
    printf("* search\n");
    printf("* tasks\n");
    
    printf("\n");
    
    printf("* builtins");
    printf("* clear\n");
    printf("* exit\n");
}

void command_cd(char *args, char *endBound) {
    char *arg1 = skipWhitespace(args);
    int arg_length = endBound - arg1;
    
    if (arg1[0] != '/' && arg1[0] != '~') {
        char *current_directory = getcwd(NULL, 0);
        char *new_directory = NULL;
        buf__fit(new_directory, strlen(current_directory) + arg_length + 1);
        
        for (int i = 0; i < strlen(current_directory); i++)
            buf_push(new_directory, current_directory[i]);
        buf_push(new_directory, '/');
        for (int i = 0; i < arg_length; i++)
            buf_push(new_directory, arg1[i]);
        buf_push(new_directory, '\0');
        
        chdir(new_directory);
    } else if ((arg_length == 1 && arg1[0] == '~')
               || (arg_length > 1 && arg1[0] == '~' && arg1[1] == '/')) {
        char *username = getenv("USER"); // TODO: Use global data instead
        
        String new_directory = NULL;
        buf__fit(new_directory, strlen(username) + 6 + arg_length + 1);
        String_appendCString(&new_directory, "/home/");
        String_appendCString(&new_directory, username);
        
        // @NOTE: Start at i = 1 so that ~ is not added to path
        for (int i = 1; i < arg_length; i++)
            buf_push(new_directory, arg1[i]);
        buf_push(new_directory, '\0');
        
        chdir(new_directory);
    } else if (arg_length > 1 && arg1[0] == '~' && arg1[1] != '/') {
        String new_directory = NULL;
        buf__fit(new_directory, 6 + arg_length + 1);
        String_appendCString(&new_directory, "/home/");
        
        // @NOTE: Start at i = 1 so that ~ is not added to path
        for (int i = 1; i < arg_length; i++)
            buf_push(new_directory, arg1[i]);
        buf_push(new_directory, '\0');
        
        chdir(new_directory);
    } else {
        char *new_directory = strToBuffer(arg1, arg_length);
        buf_push(new_directory, '\0');
        chdir(new_directory);
    }
}

void command_clear(char *args, char *endBound) {
    clrscr();
}

void command_exit(char *args, char *endBound) {
    exit(0);
}

int main() {
    // Open file
    /*if (argc > 1) {
    char *filename = args[1];
    char *fileBuffer = readFile(filename);
    if (!fileBuffer) {
    logError("", "Failed to read file.");
    return 1;
    }
    
    } else {
    logError("", "Filename must be provided.");
    return 1;
    }*/
    
    
    GlobalData globalData = {0};
    // @TODO: Unix - '/', Windows - 'C:/' (or default drive letter)
    globalData.workingDirectory = cStrToBuffer("/");
    globalData.username = cStrToBuffer(getenv("USER"));
    
    cli(&globalData);
    
    return 0;
}