#include "paled.h"

// Gives back pointer starting at next non-whitespace character that appears after start
char *skipWhitespace(char *start) {
	char *current = start;
	if (*current == '\0') return current;
    
	while (*current == ' ' || *current == '\t' || *current == '\n' || *current == '\r') {
		++current;
		if (*current == '\0') break;
	}
    
	return current;
}

// Will give back a pointer just after the word skipped.
char *skipWord(char *start, bool includeNumbers, bool includeSymbols) {
	char *current = start;
	while ((*current >= 'A' && *current <= 'Z') || (*current >= 'a' && *current <= 'z')
		   || (includeNumbers && *current >= '0' && *current <= '9')
		   || (includeNumbers && *current == '-') // Note: Technically this will also match numbers that have '-' in the middle and at end, but whatever TODO: Fix this
		   || (includeSymbols && *current >= '!' && *current <= '/')
		   || (includeSymbols && *current >= ':' && *current <= '@')
		   || (includeSymbols && *current >= '[' && *current <= '`')
		   || (includeSymbols && *current >= '{' && *current <= '~')) {
        
		++current;
        
		if (*current == '\0') break;
	}
	return current;
}

// TODO
char *skipText() {
	return NULL;
}

// TODO
char *skipSymbols() {
	return NULL;
}

char *skipNumbers(char *start) {
	char *current = start;
	if (*current == 0) return current;
    
	if (*current == '-') ++current; // Note: Not doing '+' here
	if (*current == 0) return current;
    
	while (*current >= '0' && *current <= '9') {
		++current;
		if (*current == 0) break;
	}
	return current;
}

/* Gets input, trims leading space, and puts into line char array
as long as it doesn't go over the max.

@line - array of characters to fill up
@max - maximum amount of characters allowed in line
@trimSpace - Whether to trim leading space (1) or not (0)
@return - the length of the line
*/
int parsing_getLine(char *line, int max, int trimSpace) {
	int c;
	int i = 0;
    
	/* Trim whitespace */
	while (trimSpace && ((c = getchar()) == ' ' || c == '\t'))
		;
    
	if (!trimSpace) c = getchar();
    
	/* If there's nothing left, return */
	if (c == '\n') {
		line[i] = '\0';
		return 1; /* Including \0 */
	}
    
	/* Transfer input characters into line string */
	while (c != EOF && c != '\n' && i < max)
	{
		line[i] = (char) c;
		++i;
		c = getchar();
	}
    
	/* End of string */
	line[i] = '\0';
	++i; /* Includes '\0' in the length */
    
	return i;
}

// atoi with provided length
int antoi(const char* str, int len)
{
    int i;
    int ret = 0;
    for(i = 0; i < len; ++i)
    {
        ret = ret * 10 + (str[i] - '0');
    }
    return ret;
}
