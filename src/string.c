#include "paled.h"

void String_appendCString(String *str, char *str2) {
    char *current = str2;
    while (*current != '\0') {
        buf_push(*str, *current);
        ++current;
    }
}

void String_appendString(String *str, String str2) {
    char *current = str2;
    int index = 0;
    while (index < buf_len(str2)) {
        buf_push(*str, *current);
        ++index;
        ++current;
    }
}
